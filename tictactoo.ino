#include "types.h"

void setup() {
  // put your setup code here, to run once:

}

// global state
static state_t play_field[9] = {};
static int pucks_on_stack = 5;

struct steps_t {
  int rx;
  int ry;
  int rz;
};

const static struct steps_t steps [11] = {
  {rx: 5, ry: 6, rz: 7}, // steps for FIELD0
  {rx: 5, ry: 6, rz: 7}, // steps for FIELD1
  {rx: 5, ry: 6, rz: 7}, // steps for FIELD2
  {rx: 5, ry: 6, rz: 7}, // steps for FIELD3
  {rx: 5, ry: 6, rz: 7}, // steps for FIELD4
  {rx: 5, ry: 6, rz: 7}, // steps for FIELD5
  {rx: 5, ry: 6, rz: 7}, // steps for FIELD6
  {rx: 5, ry: 6, rz: 7}, // steps for FIELD7
  {rx: 5, ry: 6, rz: 7}, // steps for FIELD8

  // {rx: 5, ry: 6, rz: 7}, // steps for STACK
  {rx: -100, ry: -100, rz: -100}, // steps for HOME
};

// on the STACK position, we have multiple chips on top of each other
// depending on how many we have already played, we have different steps to do....
const static struct steps_t stack_steps[5] = {
  {rx: 5, ry: 6, rz: 5}, // 1 puck left..
  {rx: 5, ry: 6, rz: 6}, // 2 pucks left...
  {rx: 5, ry: 6, rz: 7},
  {rx: 5, ry: 6, rz: 8},
  {rx: 5, ry: 6, rz: 9}
};


// the tic-tac-too algorithm....
field_t calculate_next_move(const state_t field[])
{
  // think really hard, and return a smart move....
  return FIELD1;
}

/* move a block to position

*/
void do_position(field_t position)
{
  int rx, ry, rz;
  if (position == STACK) {
    rx = steps[position].rx;
    ry = steps[position].ry;
    rz = steps[position].rz;
  } else {
    rx = stack_steps[pucks_on_stack - 1].rx;
    ry = stack_steps[pucks_on_stack - 1].ry;
    rz = stack_steps[pucks_on_stack - 1].rz;
  }

  move_motor_rx(rx);
  move_motor_ry(ry);
  move_motor_rz(rz);
}

void do_move(field_t position)
{
  do_position(STACK);

  // pick it
  // pick()

  do_position(position);

  // drop it..
  // drop();

  // re-align  (if needed ??)
  do_position(HOME);
}
void move_motor_rx(int steps)
{
}
void move_motor_ry(int steps)
{
}
void move_motor_rz(int steps)
{
}


int get_his_move()
{
  int new_position = FIELD5;  // return the actual new one

  // poll all read contacts, till you find a 'new' position filled

  return new_position;
}


bool myturn = false;

void play_game()
{
  if (myturn) {
    field_t my_move =  calculate_next_move(play_field);
    do_move(my_move);
    play_field[my_move] = RED;
    pucks_on_stack -= 1;
    myturn = false;
  } else {
    // poll for other move
    int his_move = get_his_move();
    play_field[his_move] = BLUE;
    myturn = true;
  }
}

// determine if we have winner
int check_winner(const state_t field[])
{
  // 0 is no winner
  // 1 RED is winner
  // 2 BLUE is winner


  // loop over the field, and check for 3-in-a-row....

  return 0;
}

void loop() {
  // put your main code here, to run repeatedly:

  // check for winner
  int winner = check_winner(play_field);

  if (!winner) {
    play_game();
  } else {
    // report YEAH or BOO...


    // reset the board
    memset(play_field, EMPTY, sizeof(play_field));
    pucks_on_stack = 5;

    // Reset the physical pieces....
    // Or do this by hand. (then wait here till all empty)
  }
}

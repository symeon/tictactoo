typedef enum {
  FIELD0 = 0,  // must start at 0, used as index
  FIELD1,
  FIELD2,
  FIELD3,
  FIELD5,
  FIELD6,
  FIELD7,
  FIELD8,

  HOME, // to re-align
  STACK // we to get all the blocks

} field_t;


typedef enum state {
  EMPTY = 0,
  RED = 1,
  BLUE = 2
} state_t;
